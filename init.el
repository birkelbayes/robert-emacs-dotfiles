;;; init.el
;;
;; This file allows Emacs to initialize my customizations
;; in Emacs lisp embedded in *one* literate Org-mode file.

;; Make startup faster by reducing the frequency of garbage
;; collection.
(setq gc-cons-threshold 4000000000
      gc-cons-percentage 0.9)

;; Load the rest of the packages
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)

;; keep the installed packages in .emacs.d
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))
(when (version< emacs-version "27.0") (package-initialize))
(defvar package--initialized)
(unless package--initialized (package-initialize t))

;; update the package metadata is the local cache is missing
(unless package-archive-contents
  (package-refresh-contents))

(require 'org)
(org-babel-load-file "~/.emacs.d/robert.org")

;; Keep emacs Custom-settings in separate file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;; Make gc pauses faster by decreasing the threshold.
 (setq gc-cons-threshold 16777216
       gc-cons-percentage 0.1)
;;; init.el ends here
(put 'dired-find-alternate-file 'disabled nil)
