(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (mc-extras pandoc-mode doom-modeline secretaria mu4e-contrib org-contacts org-mu4e esup ess-smart-underscore ess-smart-equals ess-R-data-view ess yankpad yasnippet-snippets yasnippet mu4e flyspell-popup flyspell-correct-helm flycheck magit ag neotree projectile general which-key helm doom-themes evil-escape evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-image-actual-width (quote (600)))
 '(variable-pitch ((t (:family "ETBembo")))))
